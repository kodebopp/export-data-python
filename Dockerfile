# Use an official Python base image
FROM python:3.9-slim

# Install dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        libaio1 \
        unzip \
        build-essential

# Set up the environment variables
ENV ORACLE_HOME /opt/oracle/instantclient_21_9
ENV LD_LIBRARY_PATH /opt/oracle/instantclient_21_9
ENV TNS_ADMIN /opt/oracle/instantclient_21_9/network/admin

# Copy and install Oracle Instant Client
COPY instantclient-basiclite-linuxx64.zip /opt/oracle/
RUN mkdir -p /opt/oracle && \
    cd /opt/oracle && \
    unzip instantclient-basiclite-linuxx64.zip && \
    rm -f instantclient-basiclite-linuxx64.zip && \
    echo '/opt/oracle/instantclient_21_9' | tee -a /etc/ld.so.conf.d/oracle_instant_client.conf && \
    ldconfig

# Set the working directory
WORKDIR /app

# Copy requirements.txt and install the dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy the scripts into the container
COPY . .

# Copy the entrypoint script
COPY entrypoint.sh .

# Set the entrypoint script as the entrypoint
ENTRYPOINT ["/bin/bash", "entrypoint.sh"]
