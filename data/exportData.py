import cx_Oracle
import os
import openpyxl
from openpyxl.utils import get_column_letter
import string

# Define log file name
log_file = "error.log"

# Get Oracle connection information from environment variables
host = os.environ.get("ORACLE_HOST")
port = os.environ.get("ORACLE_PORT")
service_name = os.environ.get("ORACLE_SERVICE_NAME")
user = os.environ.get("ORACLE_USER")
password = os.environ.get("ORACLE_PASSWORD")

# Create the DSN using the Oracle Service Name
dsn = cx_Oracle.makedsn(host, port, service_name=service_name)

# Connect to the Oracle database
connection = cx_Oracle.connect(user, password, dsn)
cursor = connection.cursor()

# Execute the SQL query and fetch data
query = """SELECT WORKER_ID, EMAIL_TYPE, EMAIL_ADDRESS, "PUBLIC", "PRIMARY" FROM WORKDAY_CONVERSION.V_HCM_EMAIL"""
cursor.execute(query)
data = cursor.fetchall()

# Load the Excel template
workbook = openpyxl.load_workbook("Book.xlsm", read_only=False, keep_vba=True)
worksheet = workbook.active
headers = [cell.value for cell in worksheet[1]]

# Find hidden columns
hidden_cols = set()
last_hidden = 0
for i in range(1, worksheet.max_column + 1):
    col = get_column_letter(i)
    if worksheet.column_dimensions[col].hidden:
        hidden_cols.add(col)
        last_hidden = worksheet.column_dimensions[col].max
    elif last_hidden >= i:
        hidden_cols.add(col)

# Find visible columns
visible_cols = [col for col in range(1, worksheet.max_column + 1) if get_column_letter(col) not in hidden_cols]
print("Hidden columns:\t", hidden_cols)
print("Visible columns:", visible_cols)

# Write data to worksheet and log errors
with open(log_file, "w") as f:
    for row_idx, row_data in enumerate(data, start=2):  # Assuming the data starts on row 2
        for col_idx, col in enumerate(visible_cols, start=1):
            cell_value = row_data[col_idx-1]
            col_letter = get_column_letter(col)
            if cell_value is None and col_letter not in hidden_cols:
                f.write(f"Error: Null data inserted in cell {headers[col_idx-1]} ({col_letter}{row_idx})\n")
            worksheet.cell(row=row_idx, column=col, value=cell_value)

# Save the modified Excel file
workbook.save("output.xlsm")
workbook.close()

# Close the database connection
cursor.close()
connection.close()
